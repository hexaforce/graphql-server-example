// This is the file where our generated types live (specified in our `codegen.yml` file)
import { MutationResolvers, QueryResolvers, Resolvers } from './graphql/graphql_generate'

// Use the generated `QueryResolvers` type to type check our queries!
const Query: QueryResolvers = {
  // Our third argument (`contextValue`) has a type here, so we can check the properties within our resolver's shared context value.
  books: async (_, __, { dataSources }) => {
    return dataSources.booksAPI.getBooks()
  },
}

// Use the generated `MutationResolvers` type to type check our mutations!
const Mutation: MutationResolvers = {
  // Below, we mock adding a new book. Our data set is static for this example, so we won't actually modify our data.
  addBook: async (_, { title, author }, { dataSources }) => {
    return dataSources.booksAPI.addBook({ title, author })
  },
}

// Note this "Resolvers" type isn't strictly necessary because we are already separately type checking our queries and resolvers.
// However, the "Resolvers" generated types is useful syntax if you are defining your resolvers in a single file.
const resolvers: Resolvers = { Query, Mutation }

export default resolvers
